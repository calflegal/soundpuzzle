//
//  ClipView.m
//  SoundPuzzle
//
//  Created by H Calvin Flegal on 9/22/13.
//  Copyright (c) 2013 H Calvin Flegal. All rights reserved.
//

#import "ClipView.h"


@interface ClipView()


@end


@implementation ClipView

@synthesize color = _color;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}


-(void)setColor:(UIColor *)color {
    _color = color;
    [self setNeedsDisplay];
    return;
}
-(void)setPlaying:(BOOL)playing {
    _playing = playing;
    [self setNeedsDisplay];
    return;
}
-(void) setup {
    
}
-(void)awakeFromNib {
    [self setup];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    CGRect paddedRect = CGRectMake(rect.origin.x+5, rect.origin.y+5, rect.size.width-10, rect.size.height-10);
    UIBezierPath *path = [UIBezierPath bezierPathWithOvalInRect:paddedRect];
    
    if (self.playing) {
        [[UIColor blackColor] setStroke];
        CGRect outlineRect = CGRectMake(rect.origin.x+4, rect.origin.y+4, rect.size.width-8, rect.size.height-8);
        UIBezierPath *aroundPath = [UIBezierPath bezierPathWithOvalInRect:outlineRect];
        aroundPath.lineWidth = 3.0;
        [aroundPath stroke];
        
       // [[UIColor greenColor] setFill];
    
    }
    else {
        [self.color setStroke];
        [self.color setFill];
        
    }
    [self.color setStroke];
    [self.color setFill];
    [path fill];
    [path stroke];
}



@end
