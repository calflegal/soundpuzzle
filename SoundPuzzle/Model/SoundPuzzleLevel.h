//
//  SoundPuzzleLevel.h
//  SoundPuzzle
//
//  Created by H Calvin Flegal on 9/20/13.
//  Copyright (c) 2013 H Calvin Flegal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Clip.h"
#import "AEAudioController.h"
#import <libkern/OSAtomic.h>

@interface SoundPuzzleLevel : NSObject <AEAudioPlayable> {
    int _finishedCount;
}
//of clips
@property(nonatomic,strong,readonly)NSMutableArray* clips;
//same objects as above -- the up to date shuffled que
@property(nonatomic,strong)NSMutableArray* queue;
@property(nonatomic,strong)AVAudioPlayer* sourcePlayer;

// TODO: decide how many I need herer
@property (nonatomic, readonly) NSTimeInterval duration;    //!< Length of audio, in seconds
@property (nonatomic, assign) NSTimeInterval currentTime;   //!< Current playback position, in seconds
@property (nonatomic, readwrite) BOOL channelIsPlaying;     //!< Whether the track is playing




-(id)initWithFolder:(NSString*)folderPath;
-(void)setupTapForClip:(Clip*) clip;
-(void)setupToPlayCurrentSolution;
//-(NSUInteger)indexForPlayerinQueue:(OALAudioTrack*)player;
-(Clip*)queueObjectwithCorrectIndex:(NSUInteger)correctIndex;
+(NSArray*)getArrayofMetaDataInFolder:(NSString*)folderName withMetaFile:(NSString*)fileName;


@end
