//
//  PuzzleListViewController.m
//  SoundPuzzle
//
//  Created by Calvin Flegal on 2/12/14.
//  Copyright (c) 2014 H Calvin Flegal. All rights reserved.
//

#import "PuzzleListViewController.h"
#import <AWSRuntime/AWSRuntime.h>
#import <AWSS3/AWSS3.h>
#define ACCESS_KEY_ID          @"AKIAIMZ2FTVLZDCG6ONQ"
#define SECRET_KEY             @"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"

@interface PuzzleListViewController() <UIActionSheetDelegate, AmazonServiceRequestDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong,nonatomic) NSArray* amazonListing;
@property (strong,nonatomic) NSArray* amazonFolderListing;
@property (strong,nonatomic) AmazonS3Client* client;
@property (weak,nonatomic)UITableViewCell* selectedCell;
@property (nonatomic, strong) S3TransferManager *tm;
@property(nonatomic)BOOL sourceDone;
@property(nonatomic)BOOL metaDone;
@property(nonatomic,strong)NSString* folderPath;
@property(nonatomic,strong) NSFileManager* fileManager;

@end

@implementation PuzzleListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)refreshPulled {
    [self setupAmazonListing];
    [self.tableView reloadData];
    [self.refreshControl endRefreshing];
    
}
-(BOOL) hasLocal:(UITableViewCell*)cell {
    return YES;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.client = [[AmazonS3Client alloc] initWithAccessKey:ACCESS_KEY_ID withSecretKey:SECRET_KEY];
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc]
                                        init];
    self.refreshControl = refreshControl;
    [refreshControl addTarget:self action:@selector(refreshPulled) forControlEvents:UIControlEventValueChanged];
    [self setupAmazonListing];
    self.tm = [S3TransferManager new];
    self.tm.s3 = self.client;
    self.tm.delegate = self;
    self.fileManager = [NSFileManager defaultManager];
	// Do any additional setup after loading the view.
    
}
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString* pressed = [actionSheet buttonTitleAtIndex:buttonIndex];
    
//    if ([fileManager fileExistsAtPath:txtPath] == NO) {
//        NSString *resourcePath = [[NSBundle mainBundle] pathForResource:@"vampire" ofType:nil];
//        [fileManager copyItemAtPath:resourcePath toPath:txtPath error:&error];
//    }
    if ([pressed isEqualToString:@"Cancel"]) {
        return;
    }
    else if ([pressed isEqualToString:@"Fetch"]) {
        //delete dir if it exists
        [self.fileManager removeItemAtPath:self.folderPath error:nil];
        //make folder
        [self.fileManager createDirectoryAtPath:self.folderPath withIntermediateDirectories:YES attributes:nil error:nil];
        //download that shit and chief out after done!
        self.sourceDone = NO;
        self.metaDone = NO;
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        [self.tm downloadFile:[self.folderPath stringByAppendingString:@"/source.wav"] bucket:@"soundpuzzles" key:[self.selectedCell.textLabel.text stringByAppendingString:@"/source.wav"]];
        
        [self.tm downloadFile:[self.folderPath stringByAppendingString:@"/slice.txt"] bucket:@"soundpuzzles" key:[self.selectedCell.textLabel.text stringByAppendingString:@"/slice.txt"]];
        
      //  [self.navigationController popViewControllerAnimated:YES];
    }
    else if ([pressed isEqualToString:@"Use Local"]) {
        [self.delegate initGamewithFolder:self.folderPath];
        [self.navigationController popViewControllerAnimated:YES];
    }
}
-(BOOL)localCopyExists {
    return [self.fileManager fileExistsAtPath:self.folderPath];
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //configure file system path for thre tapped cell
    self.selectedCell = [tableView cellForRowAtIndexPath:indexPath];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    self.folderPath = [documentsDirectory stringByAppendingPathComponent:self.selectedCell.textLabel.text];
    
    UIActionSheet* action = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:nil
                            destructiveButtonTitle:nil
                            otherButtonTitles:nil];
    if ([self localCopyExists]) {
        [action addButtonWithTitle:@"Use Local"];
    }
    [action addButtonWithTitle:@"Fetch"];
    [action addButtonWithTitle:@"Cancel"];
    [action showInView:self.tableView];
    
    NSLog(@"touched");
}
-(void)setupAmazonListing {
    self.amazonListing = [self.client listObjectsInBucket:@"soundpuzzles"];
    NSMutableArray* mutAmazon = [self.amazonListing mutableCopy];
    for (S3ObjectSummary* obj in self.amazonListing ) {
        if (obj.size != 0)
            [mutAmazon removeObject:obj];
    }
    self.amazonFolderListing = [mutAmazon copy];

    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return [self.amazonFolderListing count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = [self.tableView dequeueReusableCellWithIdentifier:@"puzzleName" forIndexPath:indexPath];
    S3ObjectSummary* thisobj = [self.amazonFolderListing objectAtIndex:indexPath.item];
    [cell.textLabel setText: [thisobj.key stringByReplacingOccurrencesOfString:@"/" withString:@""]];
    
    return cell;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - AmazonServiceRequestDelegate

-(void)request:(AmazonServiceRequest *)request didReceiveResponse:(NSURLResponse *)response
{
    NSLog(@"got respnse");
 //   NSDictionary * headers = ((NSHTTPURLResponse *)response).allHeaderFields;
    //if content-range is not set (this is not a range download), content-length is the length of the file

}

- (void)request:(AmazonServiceRequest *)request didReceiveData:(NSData *)data
{
    NSLog(@"data incoming");
}

-(void)request:(AmazonServiceRequest *)request didCompleteWithResponse:(AmazonServiceResponse *)response
{
    NSLog(@"Done a request");
    //hide icon and later
    if ([[((S3GetObjectRequest*)request).targetFilePath lastPathComponent] isEqualToString:@"slice.txt"]) {
        
        self.metaDone = YES;
    }
    else if ([[((S3GetObjectRequest*)request).targetFilePath lastPathComponent] isEqualToString:@"source.wav"]) {
        self.sourceDone = YES;
    }
    if (self.sourceDone && self.metaDone) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [self.delegate initGamewithFolder:self.folderPath];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)request:(AmazonServiceRequest *)request didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError called: %@", error);
    
    //stop the thing!


}

-(void)request:(AmazonServiceRequest *)request didFailWithServiceException:(NSException *)exception
{
    NSLog(@"didFailWithServiceException called: %@", exception);
}

@end
