//
//  UITouchDownGestureRecognizer.h
//  SoundPuzzle
//
//  Created by H Calvin Flegal on 11/9/13.
//  Copyright (c) 2013 H Calvin Flegal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITouchDownGestureRecognizer : UIGestureRecognizer

@end
