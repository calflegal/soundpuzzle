//
//  ClipCollectionViewCell.h
//  SoundPuzzle
//
//  Created by H Calvin Flegal on 9/22/13.
//  Copyright (c) 2013 H Calvin Flegal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ClipView.h"

@interface ClipCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet ClipView *clipView;

@end
