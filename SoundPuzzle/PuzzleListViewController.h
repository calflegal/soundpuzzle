//
//  PuzzleListViewController.h
//  SoundPuzzle
//
//  Created by Calvin Flegal on 2/12/14.
//  Copyright (c) 2014 H Calvin Flegal. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FolderSelectDelegate <NSObject>

@required

-(void)initGamewithFolder:(NSString*)path;

@end

@interface PuzzleListViewController : UITableViewController <UITableViewDataSource, UITableViewDelegate>

@property(weak,nonatomic)id <FolderSelectDelegate> delegate;

@end


