//
//  SoundPuzzleLevel.m
//  SoundPuzzle
//
//  Created by H Calvin Flegal on 9/20/13.
//  Copyright (c) 2013 H Calvin Flegal. All rights reserved.
//

//note, when a clip is tapped, lets reset the _audio struct to be emptied out etc, then we will remove after playing. When play is prssed simliar

//1. remove channel if it's there. 2. clear _audio. 3. populate audio, 4. add channel. 5. remove after?
// lets setup the audiodescription in the init block. This should also be added to clip inits to remove magic number calculations

#import "SoundPuzzleLevel.h"
#import "AEAudioFilePlayer.h"
#import "SoundPuzzleAppDelegate.h"
#import "Clip.h"
#define SAMPLE_RATE 44100 //the below will need an update too
#define BYTE_RATE 2*16/8 // bytes per sample ( 2 channels * bit depth / 8 bits per byte)
#define HEADER_LENGTH 44 //wav 16bit 441

#define SLICE_FILE @"slice.txt"

@interface SoundPuzzleLevel() {
    @protected

UInt32                        _lengthInFrames;
    int* _endingFramesArray;
    void** _dataArr;
    int _dataArrSize;

volatile int32_t              _playhead;
    BOOL _channelIsPlaying;
    int _clipsCount;
}

@property(nonatomic,strong)NSMutableArray* clips;
//of NSStrings that are hex codes
@property(nonatomic,strong)NSMutableArray* colorList;
@property(nonatomic,strong)NSString* folderName;
@property(nonatomic,strong)NSData* audioData;
@property(nonatomic)NSUInteger lastTappedIndex;
@property(nonatomic) AudioStreamBasicDescription   _audioDescription;


@end

@implementation SoundPuzzleLevel

-(void)setPlayhead:(int32_t)val {
    _playhead = val;
}


-(void)setupTapForClip:(Clip*) clip {
    if (self->_endingFramesArray) free (self->_endingFramesArray);
    self->_endingFramesArray = malloc (1*sizeof(int));
    self->_endingFramesArray[0] = clip.lengthInFrames;
    _clipsCount = 1;
    _finishedCount = 0;
    self.channelIsPlaying = NO;
    [self setPlayhead:0];
    _dataArr[0] = clip->data.mData;
    self.lastTappedIndex = [self.queue indexOfObject:clip];
    //store the index as the lastTapped. This way we know if we should begin playing or just stay stopped
    self.channelIsPlaying = YES;
}

-(void)setupToPlayCurrentSolution {
    self.channelIsPlaying = NO;
    
    
    _clipsCount = [self.queue count];
    NSLog(@"The count is %i",_clipsCount);
    //sum them up to find total length, but also setup the array of endings and the audiobufferlist
    if (self->_endingFramesArray) free(self->_endingFramesArray);
    self->_endingFramesArray = (int*) calloc(_clipsCount, sizeof(int));
    int* memToSet = self->_endingFramesArray;
    for (int i=0; i<[self.queue count]; i++) {
        Clip* thisClip = [self.queue objectAtIndex:i];
        memToSet[i] = thisClip.lengthInFrames;
        _dataArr[i] = thisClip->data.mData;
    }
    _clipsCount = [self.queue count];
    _finishedCount = 0;
    [self setPlayhead:0];
    self.channelIsPlaying = YES;
    
}
static void notifyPlaybackStopped(AEAudioController *audioController, void *userInfo, int length) {
    SoundPuzzleLevel* __autoreleasing THIS = *(SoundPuzzleLevel* __autoreleasing *)userInfo;
 //   THIS.channelIsPlaying = NO;
    //check oneoff. If its true then find the clip from lastTapped
    //otherwise, its finishedcount??
    
}

static OSStatus renderCallback(SoundPuzzleLevel *THIS,
                               AEAudioController *audioController,
                               const AudioTimeStamp *time,
                               UInt32 frames,
                               AudioBufferList *audio) {
    int32_t playhead = THIS->_playhead;
    int32_t originalPlayhead = playhead;
    
    
    
    if ( !THIS->_channelIsPlaying ) return noErr;
    
    if (!THIS->_finishedCount < THIS->_clipsCount) {
    
        if (playhead == (THIS->_endingFramesArray[THIS->_finishedCount]) ) {
            (THIS->_finishedCount)++;
            AEAudioControllerSendAsynchronousMessageToMainThread(audioController, notifyPlaybackStopped, &THIS, sizeof(SoundPuzzleLevel*));
            playhead = 0;
            if(THIS->_finishedCount == THIS->_clipsCount) THIS->_channelIsPlaying = NO;
        }
        
    }

    // Get pointers to each buffer that we can advance
    char *audioPtrs[audio->mNumberBuffers];
    for ( int i=0; i<audio->mNumberBuffers; i++ ) {
        audioPtrs[i] = audio->mBuffers[i].mData;
    }
    //there will only be one since it's interleaved.
    
    int bytesPerFrame = THIS->__audioDescription.mBytesPerFrame;
    int remainingFrames = frames;
    
    
    // The number of frames left before the end of the clip
    int framesToCopy = MIN(remainingFrames, THIS->_endingFramesArray[THIS->_finishedCount] - playhead);
    

        memcpy(audioPtrs[0], ((char*)THIS->_dataArr[THIS->_finishedCount]) + playhead * bytesPerFrame, framesToCopy * bytesPerFrame);
    
    // Advance playhead
    playhead += framesToCopy;
    
    
    OSAtomicCompareAndSwap32(originalPlayhead, playhead, &THIS->_playhead);
    
    return noErr;
}
-(AEAudioControllerRenderCallback)renderCallback {
    return &renderCallback;
}

-(NSMutableArray*)colorList {
    if (!_colorList || [_colorList count] ==0) {
        _colorList = [@[@"FF0700",@"FFD500",@"3B14AF",@"00C90D",@"FFA200",@"AEF100",
                        @"C30083",@"104BA9",@"FFE800",@"00AF64",@"FF4900",@"530FAD"] mutableCopy];
    }
    return _colorList;
}

-(id)initWithFolder:(NSString *)folderPath{
    self = [super init];
    if (self) {
        self.folderName = folderPath;
        //setup player
        self.audioData = [[NSData alloc] initWithContentsOfFile:[self.folderName stringByAppendingPathComponent:@"source.wav"]];
        self.sourcePlayer = [[AVAudioPlayer alloc] initWithData:self.audioData error:nil];
        self.clips = [[NSMutableArray alloc] init];
        //get the array of markers
        NSArray* slicePoints = [SoundPuzzleLevel getArrayofMetaDataInFolder:self.folderName withMetaFile:SLICE_FILE];
        self._audioDescription = ((SoundPuzzleAppDelegate*)(AppDelegate)).audioController.audioDescription;
        NSUInteger i = 0;
        int startOffset, rangeLength, sampleCount;
        for (NSString* slicePoint in slicePoints) {
            if ([slicePoint isEqualToString:@""]) {
                continue;
            }
            if ([slicePoints indexOfObject:slicePoint] == 0) {
                startOffset = (int) HEADER_LENGTH;
                sampleCount = (int)([slicePoint doubleValue] * SAMPLE_RATE);
                //make it bytes from samples
                rangeLength = BYTE_RATE * sampleCount;
                
            }
            else {
                NSUInteger previndex = i-1;
                startOffset = ((Clip*)([self.clips objectAtIndex:previndex])).range.location + ((Clip*)([self.clips objectAtIndex:previndex])).range.length;
                sampleCount = (int)(([slicePoint doubleValue] - [[slicePoints objectAtIndex:previndex] doubleValue]) * SAMPLE_RATE);
                //make it bytes
                rangeLength = BYTE_RATE * sampleCount;
            }
            
            int randcolor = arc4random_uniform([self.colorList count]);
            NSRange range = {startOffset,rangeLength}; //byte range for this clip
            Clip* clip= [[Clip alloc] initWithData:self.audioData andRange:range withColorCode:[self.colorList objectAtIndex:randcolor]];
            clip.correctIndex = [slicePoints indexOfObject:slicePoint]; // TODO make this part of initializer.
            [self.colorList removeObjectAtIndex:randcolor];
            [self.clips addObject:clip];
            i++;
        }
       // populate the source array randomly.
        self.queue = [self shuffleArray:[self.clips mutableCopy]];
        //allocate enough mem to make ptrs
        _dataArr = malloc(sizeof(void*) * [self.clips count]);
        _dataArrSize = [self.clips count];

    }
    return self;
    
}

- (NSMutableArray*)shuffleArray:(NSMutableArray*)array {
    
    
    for(NSUInteger i = [array count]; i > 1; i--) {
        NSUInteger j = arc4random_uniform(i);
        [array exchangeObjectAtIndex:i-1 withObjectAtIndex:j];
    }
    
    return array;
}
//-(NSUInteger)indexForPlayerinQueue:(OALAudioTrack*)player {
//    for (Clip* clip in self.queue) {
//        if ([clip.player isEqual:player] ) {
//            return [self.queue indexOfObject:clip];
//        }
//    }
//    return 0;
//}
-(Clip*)queueObjectwithCorrectIndex:(NSUInteger)correctIndex {
    for (Clip* clip in self.queue) {
        if (clip.correctIndex == correctIndex) {
            return clip;
        }
    }
    return nil;
}

+(NSArray*)getArrayofMetaDataInFolder:(NSString*)folderName withMetaFile:(NSString*)fileName {
    // read everything from text
    NSError* error =nil;
    NSString* fileContents =
    [NSString stringWithContentsOfFile:[folderName stringByAppendingPathComponent:fileName]
                              encoding:NSUTF8StringEncoding error:&error];
    
    // first, separate by new line
    NSArray* allLinedStrings =
    [fileContents componentsSeparatedByCharactersInSet:
     [NSCharacterSet newlineCharacterSet]];
    return allLinedStrings;
    
    
}
-(void)dealloc {
    for (int i=0; i< [self.clips count]; i++) {
    //    if (_dataArr[i] != NULL) free(_dataArr[i]);
    }
            
}

-(NSArray *)listFileAtPath:(NSString *)path
{
    //-----> LIST ALL FILES <-----//
    
    
    NSArray *directoryContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:path error:NULL];
    return directoryContent;
}



@end
