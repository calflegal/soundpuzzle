//
//  main.m
//  SoundPuzzle
//
//  Created by H Calvin Flegal on 9/19/13.
//  Copyright (c) 2013 H Calvin Flegal. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SoundPuzzleAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SoundPuzzleAppDelegate class]));
    }
}
