//
//  ClipCollectionViewCell.m
//  SoundPuzzle
//
//  Created by H Calvin Flegal on 9/22/13.
//  Copyright (c) 2013 H Calvin Flegal. All rights reserved.
//

#import "ClipCollectionViewCell.h"

@implementation ClipCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
