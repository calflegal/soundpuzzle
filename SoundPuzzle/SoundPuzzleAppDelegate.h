//
//  SoundPuzzleAppDelegate.h
//  SoundPuzzle
//
//  Created by H Calvin Flegal on 9/19/13.
//  Copyright (c) 2013 H Calvin Flegal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TheAmazingAudioEngine.h"
#define AppDelegate (SoundPuzzleAppDelegate *)[[UIApplication sharedApplication] delegate]


@interface SoundPuzzleAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) AEAudioController *audioController;

@end
