//
//  Clip.m
//  SoundPuzzle
//
//  Created by H Calvin Flegal on 9/19/13.
//  Copyright (c) 2013 H Calvin Flegal. All rights reserved.
//

#import "Clip.h"

@implementation Clip

-(id)initWithData:(NSData*)thedata andRange:(NSRange)range withColorCode:(NSString *)color {
    self =[super init];
    if (self) {
        //setup the raw audio buffer
        self->data.mNumberChannels = 2;
        self->data.mData = (void*)[[thedata subdataWithRange:range] bytes];
        self->data.mDataByteSize = range.length;
        self.lengthInFrames = range.length / ((SoundPuzzleAppDelegate*)AppDelegate).audioController.audioDescription.mBytesPerFrame;
        
        self.range = range;
        self.colorCode = color;
    }
    return self;
    
}
@end
