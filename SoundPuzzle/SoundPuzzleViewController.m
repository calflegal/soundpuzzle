//
//  SoundPuzzleViewController.m
//  SoundPuzzle
//
//  Created by H Calvin Flegal on 9/19/13.
//  Copyright (c) 2013 H Calvin Flegal. All rights reserved.
//
#import "SoundPuzzleAppDelegate.h"
#import "SoundPuzzleViewController.h"
#import "Clip.h"
#import "SoundPuzzleLevel.h"
#import "ClipCollectionViewCell.h"
#import "UICollectionViewDataSource_Draggable.h"
#import "UITouchDownGestureRecognizer.h"
#import "SSZipArchive.h"
#define baseAPI @"http://127.0.0.1:5000/newsong/"
#import "Toast+UIView.h"
#import "PuzzleListViewController.h"
#define APIMax




@interface SoundPuzzleViewController () <UICollectionViewDataSource,UICollectionViewDelegate,
        FolderSelectDelegate,AVAudioPlayerDelegate,UICollectionViewDataSource_Draggable,UIGestureRecognizerDelegate>
//of Clip*
@property(nonatomic,strong)SoundPuzzleLevel* level;
@property (weak, nonatomic) IBOutlet UICollectionView *pieceCollectionView;
@property(nonatomic)BOOL solutionPlaying;
@property(nonatomic,strong)NSMutableArray* playQueue;
@property (weak, nonatomic) IBOutlet UIButton *solutionPlayToggleButton;
@property (weak, nonatomic) IBOutlet UIButton *playSrcButton;
@property (weak, nonatomic) IBOutlet UILabel *artistLabel;
@property (weak, nonatomic) IBOutlet UILabel *songLabel;

@end

@implementation SoundPuzzleViewController


-(void)setSolutionPlaying:(BOOL)solutionPlaying {
    _solutionPlaying = solutionPlaying;
    [self updateUI];
    
}

-(NSMutableArray*)playQueue {
    if (!_playQueue) {
        _playQueue = [[NSMutableArray alloc] init];
    }
    return _playQueue;
}


-(void)queuePreloadTrack:(Clip*)clip {
    [self.playQueue addObject:clip];
}
-(void)startPlayQueue{
    Clip* firstClip = [self.playQueue objectAtIndex:0];
  //  [firstClip play];
    [self updateUI];
}


//set it to playing when it plays
- (IBAction)clipTapped:(UITouchDownGestureRecognizer *)gesture {
    if (self.level.sourcePlayer.playing) [self.level.sourcePlayer stop];
    CGPoint tapPoint = [gesture locationInView:self.pieceCollectionView];
    NSIndexPath* ipath = [self.pieceCollectionView indexPathForItemAtPoint:tapPoint];
    if (ipath) {
        
        NSUInteger index = [ipath item];
        Clip* clipTapped = (Clip*)[self.level.queue objectAtIndex:index];
        [self.level setupTapForClip:clipTapped];
        //interrupt the others ?
        [((SoundPuzzleAppDelegate*)AppDelegate).audioController addChannels:@[_level]];
       
    }
    
}
-(void)animateReload {
    [self.pieceCollectionView performBatchUpdates:^{
        [self.pieceCollectionView reloadSections:[NSIndexSet indexSetWithIndex:0]];
    } completion:nil];
}

- (IBAction)iGiveUpTapped:(id)sender {
    NSMutableArray* correctArray = [[NSMutableArray alloc] initWithCapacity:[self.level.queue count]];
    for (Clip* clip in self.level.queue) {
        int index = [self.level.queue indexOfObject:clip];
        [correctArray insertObject:[self.level queueObjectwithCorrectIndex:index] atIndex:index];
    }
    self.level.queue = correctArray;
    [self animateReload];
}

-(void)checkSolution {
    BOOL correct = YES;
    for (Clip* clip in self.level.queue) {
        if ([self.level.clips indexOfObject:clip] != [self.level.queue indexOfObject:clip]) {
            correct = NO;
            break;
        }
    }
    if (correct) {
    UIAlertView* alert =[[UIAlertView alloc] initWithTitle:@"Correct!" message:@"That's the order!" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    [alert show];
    }
}

#pragma mark - Button Actions
- (IBAction)playSourcePressed:(UIButton *)sender {
    if (!self.level.sourcePlayer.playing) {
        self.level.sourcePlayer.currentTime = 0;
        [self.level.sourcePlayer play];
    }
    else if (self.level.sourcePlayer.playing) {
        [self.level.sourcePlayer stop];
    }
    [self updateUI];
}
- (IBAction)playSolutionPressed:(id)sender {
  //  if (!self.solutionPlaying) {
        [self checkSolution];
        self.solutionPlaying = YES;
        [self.level setupToPlayCurrentSolution];
        [((SoundPuzzleAppDelegate*)AppDelegate).audioController addChannels:@[_level]];
 //   }
    
}
#pragma mark - UICollectionView delegate and datasource

-(UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
  
        UICollectionViewCell* cell = [self.pieceCollectionView dequeueReusableCellWithReuseIdentifier:@"Clip" forIndexPath:indexPath];
    ClipCollectionViewCell* cvc = (ClipCollectionViewCell*)cell;
    int index = indexPath.item;
    Clip* clipAtIndex =[self.level.queue objectAtIndex:index];
    cvc.clipView.color = [self colorWithHexString:clipAtIndex.colorCode];
        return cell;
        
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.level.queue count];
    
    
}
-(void)initGamewithFolder:(NSString*)path {
    self.level = nil;
    self.level = [[SoundPuzzleLevel alloc] initWithFolder:path];
    for (Clip* clip in _level.clips) {
        clip.player.delegate = self;
    }
    [self.pieceCollectionView reloadData];
    [self updateUI];
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return  1;
}
-(SoundPuzzleLevel*)level {
    
    if (!_level) {
        
        _level = [[SoundPuzzleLevel alloc] initWithFolder:[[NSBundle mainBundle] pathForResource:@"vampire" ofType:nil]];
        for (Clip* clip in _level.clips) {
            clip.player.delegate = self;
        }
    }
    return  _level;
}
-(void)updateUI {
    for (Clip* clip in self.level.queue) {
        NSUInteger clipIndex = [self.level.queue indexOfObject:clip];
        UICollectionViewCell* cell = [self.pieceCollectionView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:clipIndex inSection:0]];
        ((ClipCollectionViewCell*)(cell)).clipView.playing = clip.playing;
    }
    self.level.sourcePlayer.playing ? [self.playSrcButton setTitle:@"Stop Source" forState:UIControlStateNormal] : [self.playSrcButton setTitle:@"Play Source" forState:UIControlStateNormal] ;
}

//- (IBAction)restarthit:(id)sender {
//    [self resetPlayers];
//}


//-(void)resetPlayers {
//    self.level = [[SoundPuzzleLevel alloc] initWithFolder:@"vampire"];
//    [self animateReload];
//    [self updateUI];
//}


- (void)viewDidLoad
{
    
    [super viewDidLoad];

    //todo look at this
//    OALAudioSession* session = [[OALAudioSession alloc] init];
//    session.honorSilentSwitch =NO;
    UITouchDownGestureRecognizer *touchDown = [[UITouchDownGestureRecognizer alloc] initWithTarget:self action:@selector(clipTapped:)];
   [self.pieceCollectionView addGestureRecognizer:touchDown];
    touchDown.delegate = self;
 
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    //for loop it here to copy
    
    NSString *txtPath = [documentsDirectory stringByAppendingPathComponent:@"vampire"];
    
    if ([fileManager fileExistsAtPath:txtPath] == NO) {
        NSString *resourcePath = [[NSBundle mainBundle] pathForResource:@"vampire" ofType:nil];
        [fileManager copyItemAtPath:resourcePath toPath:txtPath error:&error];
    }
    [self initGamewithFolder:txtPath];

}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"listPuzzles"]) {
        PuzzleListViewController* nextup = (PuzzleListViewController*) segue.destinationViewController;
        nextup.delegate = self;
        
    }
}
- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillDisappear:animated];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UICollectionViewDraggable

- (BOOL)collectionView:(LSCollectionViewHelper *)collectionView canMoveItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canMoveItemAtIndexPath:(NSIndexPath *)indexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    return YES;
}

- (void)collectionView:(LSCollectionViewHelper *)collectionView moveItemAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    Clip* clipToMove = [self.level.queue objectAtIndex:fromIndexPath.item];
    [self.level.queue removeObjectAtIndex:fromIndexPath.item];
    [self.level.queue insertObject:clipToMove atIndex:toIndexPath.item];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    return YES;
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    return YES;
}

-(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}


-(void)downloadAndUnzip:(NSString *)sURL_p
{
    self.view.userInteractionEnabled = NO;
    [self.view makeToastActivity];
    dispatch_queue_t q = dispatch_queue_create("get level queue", NULL);
    dispatch_queue_t main = dispatch_get_main_queue();
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    dispatch_async(q, ^{
        //Path info
        NSURL *url = [NSURL URLWithString:sURL_p];
        NSData *data = [NSData  dataWithContentsOfURL:url];
        NSString *fileName = [[url path] lastPathComponent];
        NSString *filePath = [NSTemporaryDirectory() stringByAppendingPathComponent:fileName];
        [data writeToFile:filePath atomically:YES];
        
        //this should be async!!!
        dispatch_async(main, ^
                       {
                           
                           [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                           //Write To
                           NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                           NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
                           
                         [SSZipArchive unzipFileAtPath:filePath toDestination:documentsDirectory];
                         //  [self resetPlayers];
                           [self.view hideToastActivity];
                           self.view.userInteractionEnabled = YES;
                           [self updateUI];
                           
                       });
    });
    
}



@end
