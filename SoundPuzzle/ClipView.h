//
//  ClipView.h
//  SoundPuzzle
//
//  Created by H Calvin Flegal on 9/22/13.
//  Copyright (c) 2013 H Calvin Flegal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ClipView : UIView
@property (nonatomic,strong)UIColor* color;
@property(nonatomic,strong)NSString* shape;
@property(nonatomic)BOOL playing;

@end
