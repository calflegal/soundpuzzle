//
//  Clip.h
//  SoundPuzzle
//
//  Created by H Calvin Flegal on 9/19/13.
//  Copyright (c) 2013 H Calvin Flegal. All rights reserved.
//
#import "SoundPuzzleAppDelegate.h"
#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

@interface Clip : NSObject {
    @public AudioBuffer data;
}

@property(nonatomic,strong)NSString* colorCode;
@property(nonatomic)NSUInteger correctIndex;
@property(nonatomic)BOOL playing;
@property(nonatomic,strong)AVAudioPlayer* player;
@property(nonatomic)int lengthInFrames;




@property(nonatomic)NSRange range;





-(id)initWithData:(NSData*)data andRange:(NSRange)range withColorCode:(NSString *)color;
@end



